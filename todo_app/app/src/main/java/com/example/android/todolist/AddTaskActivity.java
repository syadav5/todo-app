/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.android.todolist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.android.todolist.data.AppExecutorsProvider;
import com.example.android.todolist.data.TaskEntry;
import com.example.android.todolist.data.TodoDatabase;

import java.util.Date;


public class AddTaskActivity extends AppCompatActivity {

    // Declare a member variable to keep track of a task's selected mPriority
    private int mPriority;
    private TodoDatabase mdb;
    private EditText mEditText;
    private Button addBtn;
    private RadioButton priorityBtn;
    private boolean isEditMode=false;
    private int taskIdToEdit=-1;
    private TaskEntry preSelectedTaskForEdit;
private static String TAG="ADD TASK";

    private void fetchTaskAndUpdateUI(final int id){

       // final LiveData<TaskEntry> preSelectedTaskForEdit = mdb.taskDao().getTaskById(id);
        AddTaskViewModelFactory addTaskFactory = new AddTaskViewModelFactory(mdb,id);
        final LiveData<TaskEntry> preSelectedTask = addTaskFactory.create(AddTaskViewModel.class).getTaskToEdit();
//        preSelectedTaskForEdit=preSelectedTask.getValue();
        preSelectedTask.observe(this,new Observer<TaskEntry>(){

            @Override
            public void onChanged(@Nullable TaskEntry taskEntry) {
                Log.d(TAG, "onChanged: Change Detected -> Going to update UI");
                preSelectedTask.removeObserver(this);
                populateUI(taskEntry);
            }
        });
/*        AppExecutorsProvider.getInstance().getDiskIO().execute(new Runnable() {
           // int id=-1;
            TaskEntry te;
            @Override
            public void run() {
                try {
                    preSelectedTaskForEdit= mdb.taskDao().getTaskById(id);

                }
                catch (Exception nfe){
                nfe.printStackTrace();
                }
                if(preSelectedTaskForEdit!=null){
                    Log.d(TAG, "run: task Retrieve successfully" +preSelectedTaskForEdit.getDescription());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            populateUI(preSelectedTaskForEdit);
                        }
                    });
                }
                else{
                    Log.d(TAG, "run: task retrieved is null.. check why?? ");
                }
            }
        });*/

        //This Executor method is not needed anymore since the task of change Detection and updating UI and running on the main thread
        //is being taken care by LiveData and Lifecycle Owner and Lifecycle observer classes/methods

        //Also, the task of running the relevant changes on the main thread are being taken automatically..

    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        mdb = TodoDatabase.getInstance(this);

        mEditText =findViewById(R.id.editTextTaskDescription);

        addBtn=findViewById(R.id.addButton);
        int chosenTaskId=isEditOperation();
        if(!(chosenTaskId<0)){
            fetchTaskAndUpdateUI(chosenTaskId);

        }
        else {
            // Initialize to highest mPriority by default (mPriority = 1)
            ((RadioButton) findViewById(R.id.radButton1)).setChecked(true);
            mPriority = 1;
        }
    }

    private int isEditOperation() {
        Intent intnt = getIntent();
        String taskId=null;
        if(intnt!=null && intnt.hasExtra(ExtrasConstant.EXTRAS_EDIT_TASK_TASKID)){
            taskId = intnt.getStringExtra(ExtrasConstant.EXTRAS_EDIT_TASK_TASKID);
            Log.d(TAG, "isEditOperation: TASK ID RECEIVED IS "+taskId);
        }
        isEditMode=taskId!=null;
       int id= taskId!=null?Integer.parseInt(taskId):-10;
        taskIdToEdit=id;
        return taskIdToEdit;
    }

    private void populateUI(TaskEntry te){
        this.mEditText.setText(te.getDescription());
       RadioButton r1= (((RadioButton) findViewById(R.id.radButton1)));
        RadioButton r2= (((RadioButton) findViewById(R.id.radButton2)));
        RadioButton r3= (((RadioButton) findViewById(R.id.radButton3)));
        String btnSelected="TASK PRIORITY = "+te.getPriority();
int selectedRadioBtnId=R.id.radButton1;
        RadioGroup rg = (RadioGroup) findViewById(R.id.radio_group);
        switch (te.getPriority()){
            case 1:
                //findViewById(R.id.radButton1).setSelected(true);
                selectedRadioBtnId=R.id.radButton1;
                ((RadioButton) findViewById(R.id.radButton1)).setSelected(true);
                mPriority=1;
                btnSelected=btnSelected+" one";
                break;
            case 2:
                //findViewById(R.id.radButton2).setSelected(true);
                selectedRadioBtnId=R.id.radButton2;
                ((RadioButton) findViewById(R.id.radButton2)).setSelected(true);
                mPriority=2;
                btnSelected=btnSelected+" two";
                break;
            case 3:
                selectedRadioBtnId=R.id.radButton3;
                //findViewById(R.id.radButton3).setSelected(true);
                ((RadioButton) findViewById(R.id.radButton3)).setSelected(true);
                btnSelected=btnSelected+" THree";
                mPriority=3;
                break;
            default:
                //findViewById(R.id.radButton1).setSelected(true);
                selectedRadioBtnId=R.id.radButton1;
                ((RadioButton) findViewById(R.id.radButton1)).setSelected(true);

                mPriority=1;
                btnSelected=btnSelected+" ONE-DEFAULT";
                break;
        }
        Log.d(TAG, "populateUI: task sel "+btnSelected);
        rg.check(selectedRadioBtnId);
        this.addBtn.setText("UPDATE");
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putInt(ExtrasConstant.TASKID_TO_EDIT,this.taskIdToEdit);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    /**
     * onClickAddTask is called when the "ADD" button is clicked.
     * It retrieves user input and inserts that new task data into the underlying database.
     */
    public void onClickAddTask(View view) {
        // Not yet implemented
        String desc =  mEditText.getText().toString();

        final TaskEntry task = isEditMode?new TaskEntry(taskIdToEdit,desc,mPriority,new Date()):new TaskEntry(desc,mPriority,new Date());
        final long output;
        AppExecutorsProvider executorsProvider = AppExecutorsProvider.getInstance();
        executorsProvider.getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                mdb = TodoDatabase.getInstance(getApplicationContext());
                if(isEditMode) {
                    int output = mdb.taskDao().updateTask(task);
                    Log.d(TAG, "onClickAddTask: task UPDATED with output = "+output);

                }
                else{
                   long output =mdb.taskDao().addTask(task);
                    Log.d(TAG, "onClickAddTask: task created with output = "+output);

                }
                finish();
                Log.d(TAG, "run: ran main thread... "+(isEditMode?"EDIT":"ADD"));

            }
        });

    }


    /**
     * onPrioritySelected is called whenever a priority button is clicked.
     * It changes the value of mPriority based on the selected button.
     */
    public void onPrioritySelected(View view) {
        if (((RadioButton) findViewById(R.id.radButton1)).isChecked()) {
            mPriority = 1;
        } else if (((RadioButton) findViewById(R.id.radButton2)).isChecked()) {
            mPriority = 2;
        } else if (((RadioButton) findViewById(R.id.radButton3)).isChecked()) {
            mPriority = 3;
        }
    }
}
