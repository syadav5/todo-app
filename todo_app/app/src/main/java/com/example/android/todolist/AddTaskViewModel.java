package com.example.android.todolist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.android.todolist.data.TaskEntry;
import com.example.android.todolist.data.TodoDatabase;

/**
 * Created by Krishna on 2018-06-10.
 */

public class AddTaskViewModel extends ViewModel{
    LiveData<TaskEntry> taskToEdit;
    public AddTaskViewModel(TodoDatabase mTodoDb, int mTaskId){
      this.taskToEdit=mTodoDb.taskDao().getTaskById(mTaskId);
     }
    public LiveData<TaskEntry> getTaskToEdit(){
        return this.taskToEdit;
    }
}
