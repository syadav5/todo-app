package com.example.android.todolist;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.android.todolist.data.TaskEntry;
import com.example.android.todolist.data.TodoDatabase;

import java.util.List;

/**
 * Created by Krishna on 2018-06-10.
 */

public class MainActivityViewModel extends AndroidViewModel {
    private LiveData<List<TaskEntry>> listOfTasks;
    private String TAG="MAIN VIEW MODEL";
    public MainActivityViewModel(@NonNull Application application){
        super(application);
        TodoDatabase todoDatabase = TodoDatabase.getInstance(this.getApplication());
        Log.d(TAG, "MainActivityViewModel: Going to make DBCALL");
        this.listOfTasks=  todoDatabase.taskDao().getAllTasks();
    }
    public LiveData<List<TaskEntry>>  getAllTasks(){
        return this.listOfTasks;
    }
}
