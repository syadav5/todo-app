package com.example.android.todolist.data;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by Krishna on 2018-06-09.
 */

public class DateLongTypeConverter {
    @TypeConverter
    public static Date toDate(Long dt){
        return dt!=null? new Date(dt):null;
    }
    @TypeConverter
    public static Long toLong(Date dat){
        return dat!=null?dat.getTime():null;
    }

}
