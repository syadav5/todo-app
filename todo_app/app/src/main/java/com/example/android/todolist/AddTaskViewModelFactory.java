package com.example.android.todolist;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.android.todolist.data.TodoDatabase;

/**
 * Created by Krishna on 2018-06-10.
 */

public class AddTaskViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final TodoDatabase mTodoDatabase;
    private final int mTaskId;

    public AddTaskViewModelFactory(TodoDatabase mTodoDatabase,int mTaskId){
        this.mTodoDatabase=mTodoDatabase;
        this.mTaskId=mTaskId;
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new AddTaskViewModel(mTodoDatabase,mTaskId);
    }
}
