package com.example.android.todolist;

/**
 * Created by Krishna on 2018-06-09.
 */

public class ExtrasConstant {
    public static final String EXTRAS_EDIT_TASK_TASKID="EXTRAS_EDIT_TASK_TASKID";
    public static final String TASKID_TO_EDIT="TASKID_TO_EDIT";

}
