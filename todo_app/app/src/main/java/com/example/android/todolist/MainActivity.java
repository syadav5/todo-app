/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.android.todolist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.example.android.todolist.data.AppExecutorsProvider;
import com.example.android.todolist.data.CustomListAdapter;
import com.example.android.todolist.data.TaskEntry;
import com.example.android.todolist.data.TodoDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//
//public class MainActivity extends AppCompatActivity implements
//        LoaderManager.LoaderCallbacks<Cursor> {

public class MainActivity extends AppCompatActivity{

    // Constants for logging and referring to a unique loader
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int TASK_LOADER_ID = 0;
    private CustomListAdapter mAdapterList;
   private  RecyclerView mRecyclerView;
    private TodoDatabase mdb ;
    private List<TaskEntry> dataList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewTasks);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapterList= new CustomListAdapter(this,null);
        mRecyclerView.setAdapter(mAdapterList);
        mdb = TodoDatabase.getInstance(getApplicationContext());
        getListOfTasksFromViewModel();


        /*
         Add a touch helper to the RecyclerView to recognize when a user swipes to delete an item.
         An ItemTouchHelper enables touch behavior (like swipe and move) on each ViewHolder,
         and uses callbacks to signal when a user is performing these actions.
         */

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            // Called when a user swipes left or right on a ViewHolder
            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int swipeDir) {
                // Here is where you'll implement swipe to delete
                AppExecutorsProvider.getInstance().getDiskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "run: deleting on swipe..."+swipeDir);
                       int pos= viewHolder.getAdapterPosition();
                        Log.d(TAG, "run: ITEM TO BE DELETED IS "+mAdapterList.getTaskList().get(pos).getDescription());
                       TaskEntry item= mAdapterList.getTaskList().get(pos);
                        mdb.taskDao().deleteTask(mAdapterList.getTaskList().get(pos));
                        //fetchDataFromDatabase();
                    }
                });
            }

        }).attachToRecyclerView(mRecyclerView);

        /*
         Set the Floating Action Button (FAB) to its corresponding View.
         Attach an OnClickListener to it, so that when it's clicked, a new intent will be created
         to launch the AddTaskActivity.
         */
        FloatingActionButton fabButton = (FloatingActionButton) findViewById(R.id.fab);

        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a new intent to start an AddTaskActivity
                Intent addTaskIntent = new Intent(MainActivity.this, AddTaskActivity.class);
                startActivity(addTaskIntent);
            }
        });

        /*
         Ensure a loader is initialized and active. If the loader doesn't already exist, one is
         created, otherwise the last created loader is re-used.
         */
        //getSupportLoaderManager().initLoader(TASK_LOADER_ID, null, this);
    }


    /**
     * This method is called after this activity has been paused or restarted.
     * Often, this is after new data has been inserted through an AddTaskActivity,
     * so this restarts the loader to re-query the underlying data for any changes.
     */
    @Override
    protected void onResume() {


        super.onResume();
       // fetchDataFromDatabase();

        // re-queries for all tasks
       // getSupportLoaderManager().restartLoader(TASK_LOADER_ID, null, this);
    }
private void getListOfTasksFromViewModel(){

    /*AppExecutorsProvider executorsProvider = AppExecutorsProvider.getInstance();
    executorsProvider.getDiskIO().execute(new Runnable() {
        @Override
        public void run() {
            LiveData<List<TaskEntry>> teLiveData=mdb.taskDao().getAllTasks();
            dataList=teLiveData.getValue();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mAdapterList!=null)
                    mAdapterList.setTaskList(dataList);
                    //setTaskLists(dataList);
                    Log.d(TAG, "run: ran main thread... "+dataList);
                    mAdapterList.notifyDataSetChanged();
                    mRecyclerView.invalidate();
                }
            });

        }
    });*/

    MainActivityViewModel mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
    //Live Data runs outside of main Thread, therefore no need to use EXecutor
    mainActivityViewModel.getAllTasks().observe(this, new Observer<List<TaskEntry>>() {
        @Override
        public void onChanged(@Nullable List<TaskEntry> taskEntries) {
            //This runs on main UI thread by default, therefore no need to run it explicitly on Main UI thread..
            Log.d(TAG, "onChanged: Change Detected in main activity get all tasks..");
            dataList=taskEntries;
            if(mAdapterList!=null)
                mAdapterList.setTaskList(taskEntries);
            //setTaskLists(dataList);
            Log.d(TAG, "run: Ran again using Live Data... "+dataList);

        }
    });
    /*runOnUiThread(new Runnable() {
        @Override
        public void run() {
            if(mAdapterList!=null)
                mAdapterList.setTaskList(dataList);
            //setTaskLists(dataList);
            Log.d(TAG, "run: ran main thread... "+dataList);
            mAdapterList.notifyDataSetChanged();
            mRecyclerView.invalidate();
        }
    });

*/

}
}

