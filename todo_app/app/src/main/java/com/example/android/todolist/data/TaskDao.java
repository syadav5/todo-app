package com.example.android.todolist.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Krishna on 2018-06-08.
 */
@Dao
public interface TaskDao {

    @Query("select * from task ORDER BY priority")
    public LiveData<List<TaskEntry>> getAllTasks();
    @Delete
    public void deleteTask(TaskEntry id);
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public int updateTask(TaskEntry newTask);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long addTask(TaskEntry newEntry);

    @Query("select * from task where id=:id")
    public LiveData<TaskEntry> getTaskById(int id);


}

