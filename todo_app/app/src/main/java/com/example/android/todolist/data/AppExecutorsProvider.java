package com.example.android.todolist.data;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Krishna on 2018-06-09.
 */

public class AppExecutorsProvider  {
    private Executor diskIO;
    private Executor networkIO;
    private Executor mainThreadExecutor;
    private static Object LOCK= new Object();

    private static AppExecutorsProvider appInstance;

    public static AppExecutorsProvider getInstance() {
        if(appInstance == null){
            synchronized (LOCK){
                appInstance=new AppExecutorsProvider(Executors.newSingleThreadExecutor(),Executors.newFixedThreadPool(3),new MainThreadExecutor());
            }
        }
        return appInstance;
    }

    public Executor getDiskIO() {
        return diskIO;
    }

    public void setDiskIO(Executor diskIO) {
        this.diskIO = diskIO;
    }

    public Executor getNetworkIO() {
        return networkIO;
    }

    public void setNetworkIO(Executor networkIO) {
        this.networkIO = networkIO;
    }

    public Executor getMainThreadExecutor() {
        return this.mainThreadExecutor;
    }

    public void setMainThreadExecutor(Executor mainThreadExecutor) {
        this.mainThreadExecutor = mainThreadExecutor;
    }

    public AppExecutorsProvider(Executor diskIO, Executor networkIO, Executor mainThreadExecutor) {
        this.diskIO = diskIO;
        this.networkIO = networkIO;
        this.mainThreadExecutor = mainThreadExecutor;
    }

    private static class MainThreadExecutor implements Executor {
private Handler handler = new Handler(Looper.getMainLooper());
        @Override
        public void execute(Runnable command) {

            handler.post(command);
        }
    }
}
