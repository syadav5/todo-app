package com.example.android.todolist.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.util.Log;

/**
 * Created by Krishna on 2018-06-08.
 */
@Database(
        entities = {TaskEntry.class},version = 1, exportSchema = false
)
public abstract class TodoDatabase extends RoomDatabase {
  private static TodoDatabase instance=null;
    private static String TAG="TodoDatabase";

    private static Object LOCK = new Object();
    public static TodoDatabase getInstance(Context context){
        if(instance==null){
            synchronized (LOCK){
                if(instance==null){
                    instance= Room.databaseBuilder(
                            context.getApplicationContext(),TodoDatabase.class,DBConstants.TASKLIST_DATABASE_NAME).build();
                }
            }
        }
        Log.d(TAG, "getInstance: creating database");
            return instance;
    }
public abstract TaskDao taskDao();
}
