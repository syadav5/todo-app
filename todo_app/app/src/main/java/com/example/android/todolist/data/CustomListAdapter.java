package com.example.android.todolist.data;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.example.android.todolist.AddTaskActivity;
import com.example.android.todolist.ExtrasConstant;
import com.example.android.todolist.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 2018-06-09.
 */

public class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.TaskListViewHolder> {

    private List<TaskEntry> mTaskList= new ArrayList<>();
    private Context mContext;
    private ItemClickListener mItemClickListener;
    private final class MyOnClickListener implements OnClickListener{

        @Override
        public void onClick(View v) {
            Log.d("", "onClick: dont know whats goingon");
        }
    }
    public interface ItemClickListener extends AdapterView.OnItemClickListener {}
    public void setTaskList(List<TaskEntry> taskList){
        this.mTaskList=taskList;
        notifyDataSetChanged();
    }
    public List<TaskEntry> getTaskList(){
        return this.mTaskList;
    }
    public CustomListAdapter(Context context,ItemClickListener onClickListener){
        super();
        this.mContext=context;
        this.mItemClickListener=onClickListener;
    }
    @NonNull
    @Override
    public TaskListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_layout, parent, false);
        view.setOnClickListener(new MyOnClickListener());

        return new TaskListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskListViewHolder holder, int position) {

        TaskEntry task = mTaskList.get(position);
        holder.taskDescriptionView.setText(task.getDescription());
        holder.priorityView.setText(""+task.getPriority());

      //  holder.priorityView.setText(""+task.getPriority());
     }

    @Override
    public int getItemCount() {
        return this.mTaskList.size();
    }


    private int getPriorityColor(int priority) {
        int priorityColor = 0;

        switch(priority) {
            case 1: priorityColor = ContextCompat.getColor(mContext, R.color.materialRed);
                break;
            case 2: priorityColor = ContextCompat.getColor(mContext, R.color.materialOrange);
                break;
            case 3: priorityColor = ContextCompat.getColor(mContext, R.color.materialYellow);
                break;
            default:
                priorityColor=ContextCompat.getColor(mContext,R.color.colorPrimary);
                break;
        }
        return priorityColor;
    }




    class TaskListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Class variables for the task description and priority TextViews
        TextView taskDescriptionView;
        TextView priorityView;

        public TaskListViewHolder(View itemView) {
            super(itemView);

            taskDescriptionView = (TextView) itemView.findViewById(R.id.taskDescription);
            priorityView = (TextView) itemView.findViewById(R.id.priorityTextView);
itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           int id= mTaskList.get(getAdapterPosition()).getId();
            Log.d("", "onItemClick: Item Clicked == "+id);
            Intent editActivityIntent = new Intent(v.getContext(),AddTaskActivity.class);
            editActivityIntent.putExtra(ExtrasConstant.EXTRAS_EDIT_TASK_TASKID,""+id);
            v.getContext().startActivity(editActivityIntent);
//            mItemClickListener.onItemClick(null,v,getAdapterPosition(),id);
        }
    }
}

